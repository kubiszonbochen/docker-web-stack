vcl 4.0;
backend wordpress1 {
    .host = "192.168.10.4";
    .port = "80";
 }
backend wordpress2 {
    .host = "192.168.10.5";
    .port = "80";
 }
sub vcl_recv {
    if (req.url ~ "^/java/") {
        set req.backend_hint = wordpress1;
    } else {
        set req.backend_hint = wordpress2;
    }
}
sub vcl_deliver {
    unset resp.http.X-Varnish;
    unset resp.http.Via;
    unset resp.http.Age;
    unset resp.http.X-Powered-By;
    unset resp.http.Server;
    set resp.http.Server = "wordpress_od_XXX";
    unset resp.http.X-Cacheable;
    unset resp.http.Pragma;
}
